package de.fzk.iai.ilcd.zip;

import de.schlichtherle.io.File;
import de.schlichtherle.io.FileInputStream;
import de.schlichtherle.io.FileWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.filechooser.FileFilter;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class wraps around a ZIP file representing an ILCD archive.
 */
/**
 * @author oli
 * 
 */
public class ILCDZip extends de.schlichtherle.io.File {

	private static final long serialVersionUID = 1L;

	private Logger log = LogManager.getLogger(this.getClass());

	// public static final String FILE_SEPARATOR = File.separator;

	/**
	 * 
	 */
	public static final String PATH_PREFIX = File.separator + "ILCD" + File.separator;

	public static final String MANIFEST_PATH = File.separator + "META-INF" + File.separator + "MANIFEST.MF";

	/**
	 * @param fileName
	 */
	public ILCDZip(String fileName) {
		super(fileName);
	}

	/**
	 * @param file
	 */
	public ILCDZip(File file) {
		super(file);
	}

	/**
	 * @param file
	 */
	public ILCDZip(java.io.File file) {
		super(file);
	}

	/**
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public InputStream getFile(String path) throws IOException {
		// if no leading slash is present, add one
		if (!path.startsWith(PATH_PREFIX))
			path = PATH_PREFIX + path;

		log.debug("getting file " + path + " from " + this.getAbsolutePath());
		return new FileInputStream(new File(this.getAbsolutePath() + path));
	}

	/**
	 * Checks if the archive is valid. This is the case if all of the following
	 * conditions are met: - the archive is readable - it contains a directory
	 * 'ILCD' at root level - it contains a manifest file which specifies an
	 * 'ILCD-Version' attribute
	 * 
	 * @return true if the archive is valid, false otherwise
	 */
	public boolean validate() {
		boolean result = false;

		log.info("validating archive " + this.getAbsolutePath());

		try {
			boolean versionPresent = false;
			boolean ILCDDirPresent = false;

			// ILCD version must be specified
			if (this.getILCDVersion() != null)
				versionPresent = true;

			log.info("ILCD version present: " + (this.getILCDVersion() != null ? "OK" : "FAILED"));

			// directory "ILCD" must be present
			File ILCDDir = new File(this.getAbsoluteFile() + PATH_PREFIX);
			ILCDDirPresent = ILCDDir.exists();

			log.info("directory 'ILCD' present: " + (ILCDDirPresent ? "OK" : "FAILED"));

			if (versionPresent && ILCDDirPresent)
				result = true;

		} catch (Exception e) {
			log.error(e);
			result = false;
		}

		log.info("archive is " + (result ? "VALID" : "INVALID"));
		return result;
	}

	/**
	 * @return
	 * @throws InvalidArchiveException
	 */
	public ILCDManifest getManifest() throws InvalidArchiveException {
		InputStream is = null;
		ILCDManifest manifest = null;
		try {
			log.debug("trying to retrieve manifest file from " + this.getAbsolutePath() + MANIFEST_PATH);
			File manifestFile = new File(this.getAbsolutePath() + MANIFEST_PATH);
			is = new FileInputStream(manifestFile);
			manifest = new ILCDManifest(is);
		} catch (Exception e) {
			log.error(e);
			throw new InvalidArchiveException("Cannot retrieve manifest file", e);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
			}
		}
		return manifest;
	}

	/**
	 * @param manifest
	 */
	public void setManifest(ILCDManifest manifest) {
		try {
			File file = new File(this.getAbsolutePath() + MANIFEST_PATH);
			FileWriter fw = new FileWriter(file);
			fw.write(manifest.toString());
			fw.close();
			File.update();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return
	 * @throws InvalidArchiveException
	 */
	public String getILCDVersion() throws InvalidArchiveException {
		return this.getManifest().getILCDVersion();
	}

	/**
	 * @return
	 */
	public static FileFilter ZipFileFilter() {
		return new FileFilter() {
			public boolean accept(java.io.File f) {
				return f.isDirectory() || f.getName().toLowerCase().endsWith(".zip");
			}

			public String getDescription() {
				return "ILCD Archives (*.zip)";
			}
		};

	}
}
