package de.fzk.iai.ilcd.zip;

public class InvalidArchiveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidArchiveException() {
		super();
	}

	public InvalidArchiveException(String msg) {
		super(msg);
	}

	public InvalidArchiveException(Exception e) {
		super(e);
	}

	public InvalidArchiveException(String msg, Exception e) {
		super(msg, e);
	}
}
