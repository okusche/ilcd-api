package de.fzk.iai.ilcd.api.binding.helper;

import de.fzk.iai.ilcd.api.Constants;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.util.HashMap;
import java.util.Map;

public class ContextFactory {

	protected static final Logger log = org.apache.logging.log4j.LogManager.getLogger(ContextFactory.class);

	private static ContextFactory _instance = null;

	private Map<ILCDTypes, JAXBContext> contexts = null;

	public static ContextFactory getInstance() {
		if (_instance == null) {
			_instance = new ContextFactory();
		}
		return _instance;
	}

	public ContextFactory() {
		this.contexts = new HashMap<ILCDTypes, JAXBContext>();
	}

	public JAXBContext getContext(ILCDTypes type) {

		if (!this.contexts.containsKey(type)) {
			JAXBContext jc = null;
			try {
				switch (type) {
				case CONTACT:
					jc = JAXBContext.newInstance(Constants.PACKAGE_CONTACT);
					break;
				case FLOW:
					jc = JAXBContext.newInstance(Constants.PACKAGE_FLOW);
					break;
				case FLOWPROPERTY:
					jc = JAXBContext.newInstance(Constants.PACKAGE_FLOWPROPERTY);
					break;
				case LCIAMETHOD:
					jc = JAXBContext.newInstance(Constants.PACKAGE_LCIAMETHOD);
					break;
				case PROCESS:
					jc = JAXBContext.newInstance(Constants.PACKAGE_PROCESS + ":" + Constants.EXTENSION_PACKAGE_PRODUCTMODEL + ":" + Constants.EXTENSION_PACKAGE_ORIGINATINGPROCESS);
					break;
				case SOURCE:
					jc = JAXBContext.newInstance(Constants.PACKAGE_SOURCE);
					break;
				case UNITGROUP:
					jc = JAXBContext.newInstance(Constants.PACKAGE_UNITGROUP);
					break;
				case COMMON:
					jc = JAXBContext.newInstance(Constants.PACKAGE_COMMON);
					break;
				case CATEGORIES:
					jc = JAXBContext.newInstance(Constants.PACKAGE_CATEGORIES);
					break;
				case LOCATIONS:
					jc = JAXBContext.newInstance(Constants.PACKAGE_LOCATIONS);
					break;
				case LCIAMETHODOLOGIES:
					jc = JAXBContext.newInstance(Constants.PACKAGE_LCIAMETHODOLOGIES);
					break;
				default:
					log.error("Context lookup failed, invalid type.");
					break;
				}
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			this.contexts.put(type, jc);
			return jc;
		}

		return (JAXBContext) this.contexts.get(type);

	}

}
