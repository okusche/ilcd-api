package de.fzk.iai.ilcd.api.extension.ef.originatingprocess;

import de.fzk.iai.ilcd.api.app.common.GlobalReference;
import de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceTypeValues;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "originatingProcess", namespace = "http://eplca.jrc.ec.europa.eu/ILCD/Extensions/2018/EF")
@XmlAccessorType(XmlAccessType.FIELD)
public class OriginatingProcess extends GlobalReference {

	@Override
	public GlobalReferenceTypeValues getType() {
		return GlobalReferenceTypeValues.PROCESS_DATA_SET;
	}

	@XmlAttribute
	protected String flowUUID;

	public String getFlowUUID() {
		return flowUUID;
	}

	public void setFlowUUID(String flowUUID) {
		this.flowUUID = flowUUID; 
	}
}
