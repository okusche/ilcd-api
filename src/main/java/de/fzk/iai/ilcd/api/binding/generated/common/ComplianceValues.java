//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-34 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.03.01 at 01:55:00 PM MEZ 
//

package de.fzk.iai.ilcd.api.binding.generated.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ComplianceValues.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="ComplianceValues"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Fully compliant"/&gt;
 *     &lt;enumeration value="Not compliant"/&gt;
 *     &lt;enumeration value="Not defined"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ComplianceValues")
@XmlEnum
public enum ComplianceValues {

	/**
	 * Meets all requirements of this compliance aspect as defined in the
	 * respective "Compliance system".
	 * 
	 */
	@XmlEnumValue("Fully compliant")
	FULLY_COMPLIANT("Fully compliant"),

	/**
	 * Does not meet all requirements of this compliance aspect, as defined for
	 * the respective "Compliance system".
	 * 
	 */
	@XmlEnumValue("Not compliant")
	NOT_COMPLIANT("Not compliant"),

	/**
	 * For this compliance aspect the named "Compliance system" has not defined
	 * compliance requirements.
	 * 
	 */
	@XmlEnumValue("Not defined")
	NOT_DEFINED("Not defined");
	private final String value;

	ComplianceValues(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static ComplianceValues fromValue(String v) {
		for (ComplianceValues c : ComplianceValues.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

	public String getValue() {
		return value;
	}

}
