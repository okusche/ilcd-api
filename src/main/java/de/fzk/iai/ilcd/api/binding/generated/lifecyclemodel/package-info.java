//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.10.02 at 08:01:11 PM CEST 
//

@javax.xml.bind.annotation.XmlSchema(namespace = "http://eplca.jrc.ec.europa.eu/ILCD/LifeCycleModel/2017", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package de.fzk.iai.ilcd.api.binding.generated.lifecyclemodel;
