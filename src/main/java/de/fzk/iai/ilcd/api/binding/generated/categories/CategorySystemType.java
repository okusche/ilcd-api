//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-34 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.03.01 at 01:55:00 PM MEZ 
//

package de.fzk.iai.ilcd.api.binding.generated.categories;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import de.fzk.iai.ilcd.api.app.categories.Categories;
import de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceType;

/**
 * <p>
 * Java class for CategorySystemType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CategorySystemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="referenceToSource" type="{http://lca.jrc.it/ILCD/Common}GlobalReferenceType" minOccurs="0"/&gt;
 *         &lt;element name="categories" type="{http://lca.jrc.it/ILCD/Categories}CategoriesType" maxOccurs="7"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategorySystemType", propOrder = { "referenceToSource", "categories" })
public class CategorySystemType {

	@XmlElement(type = de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceType.class)
	protected GlobalReferenceType referenceToSource;

	@XmlElement(required = true, type = Categories.class)
	protected List<CategoriesType> categories;

	@XmlAttribute
	protected String name;

	/**
	 * Gets the value of the referenceToSource property.
	 * 
	 * @return possible object is {@link GlobalReferenceType }
	 * 
	 */
	public GlobalReferenceType getReferenceToSource() {
		return referenceToSource;
	}

	/**
	 * Sets the value of the referenceToSource property.
	 * 
	 * @param value
	 *            allowed object is {@link GlobalReferenceType }
	 * 
	 */
	public void setReferenceToSource(GlobalReferenceType value) {
		this.referenceToSource = value;
	}

	/**
	 * Gets the value of the categories property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the categories property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCategories().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CategoriesType }
	 * 
	 * 
	 */
	public List<CategoriesType> getCategories() {
		if (categories == null) {
			categories = new ArrayList<CategoriesType>();
		}
		return this.categories;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

}
