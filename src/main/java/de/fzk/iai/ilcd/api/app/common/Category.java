package de.fzk.iai.ilcd.api.app.common;

import java.math.BigInteger;

public class Category extends de.fzk.iai.ilcd.api.binding.generated.common.CategoryType {

	public Category() {
	}

	public Category(int level, String value) {
		this.level = BigInteger.valueOf(level);
		this.value = value;
	}

	public boolean equals(Category cat) {
		return (this.level.equals(cat.getLevel()) && this.getValue().equals(cat.getValue()));
	}

}
