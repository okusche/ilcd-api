package de.fzk.iai.ilcd.api.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MultiLangUtil {

	protected static Logger logger = LogManager.getLogger(MultiLangUtil.class);

	/**
	 * Set the string value of a multilang string
	 * 
	 * example: MultiLangUtil.setString(theList, "foo", "en",
	 * de.fzk.iai.ilcd.api.app.common.FTMultiLang.class);
	 * 
	 * @param list
	 * @param value
	 * @param language identifier
	 * @param clazz
	 *            the MultiLang class (StringMultiLang, STMultiLang,
	 *            FTMultiLang)
	 */
	public static void setString(List<Object> list, String value, String language, Class<?> clazz) {
		logger.debug("setting " + clazz.getCanonicalName());

		// if List is non-existent, create one
		if (list == null) {
			list = new ArrayList<Object>();
		} else {
			try {
				// remove any pre-existing entries of the same language
				try {
					for (Iterator<Object> it = list.iterator(); it.hasNext();) {
						Object item = it.next();
						item = clazz.cast(item);
						Method methodGetLang = item.getClass().getMethod("getLang", (Class[]) null);
						String lang = (String) methodGetLang.invoke(item, (Object[]) null);
						if (lang.equals(language)) {
							list.remove(item);
							break;
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (value != null) {
					// set value
					Constructor<?> constructor = clazz.getConstructor((Class[]) null);
					Object newItem = constructor.newInstance((Object[]) null);
					Method setLangMethod = newItem.getClass().getMethod("setLang", String.class);
					setLangMethod.invoke(newItem, new Object[] { language });
					Method setValueMethod = newItem.getClass().getMethod("setValue", String.class);
					setValueMethod.invoke(newItem, new Object[] { value });
					list.add(newItem);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Get the string value of a multilang string
	 * 
	 * @param list
	 * @param language identifier
	 * @return the string value
	 */
	public static String getString(List<?> list, String language) {
		if (!list.isEmpty()) {
			try {
				for (Iterator<?> it = list.iterator(); it.hasNext();) {
					Object item = it.next();

					Method getLangMethod = item.getClass().getMethod("getLang", (Class[]) null);
					String lang = (String) getLangMethod.invoke(item, (Object[]) null);

					if (lang.equals(language)) {
						Method getValueMethod = item.getClass().getMethod("getValue", (Class[]) null);
						return (String) getValueMethod.invoke(item, (Object[]) null);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;

	}
}
