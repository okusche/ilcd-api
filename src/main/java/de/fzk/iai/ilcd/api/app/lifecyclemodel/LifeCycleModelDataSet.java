package de.fzk.iai.ilcd.api.app.lifecyclemodel;

import de.fzk.iai.ilcd.api.binding.generated.common.*;
import de.fzk.iai.ilcd.api.binding.generated.lifecyclemodel.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigInteger;
import java.util.*;

/**
 * <p>
 * The main purpose of the methods below is to adapt the LifeCycleModelDataSet
 * Object (XML compatible) with LifeCycleModel Entity (SQL compatible )
 * </p>
 * 
 * At the time of writing, the Elvis operator has not been implemented in java.
 * 
 * <blockquote>I'm limited by the technology of my time.</blockquote>
 * 
 * see de.iai.ilcd.model.lifecycle.LifeCycleModel
 * 
 * @author MK
 * @since soda4LCA 5.7.0
 *
 */
@XmlRootElement(name = "lifeCycleModelDataSet", namespace = "http://eplca.jrc.ec.europa.eu/ILCD/LifeCycleModel/2017")
public class LifeCycleModelDataSet extends LifeCycleModelDataSetType {
	public String getNullableSchemaVersion() {
		return Optional.ofNullable(this).map(LifeCycleModelDataSet::getVersion).orElse(null);
	}

	public String getNullableLocations() {
		return Optional.ofNullable(this).map(LifeCycleModelDataSet::getLocations).orElse(null);
	}

	// === DataSetInformation === //

	private Optional<DataSetInformationType> optionalDSItype() {
		return Optional.ofNullable(this).map(LifeCycleModelDataSet::getLifeCycleModelInformation)
				.map(LifeCycleModelInformationType::getDataSetInformation);
	}

	public String getNullableUUID() {
		return optionalDSItype().map(DataSetInformationType::getUUID).orElse(null);
	}

	public Map<String, String> safeGetBaseName() {
		return optionalDSItype().map(DataSetInformationType::getName).map(NameType::getBaseName)
				.map(LifeCycleModelDataSet::adaptMultiLang).orElse(new HashMap<>());
	}

	public Map<String, String> safeGetTreatmentStandardsRoutes() {

		return optionalDSItype().map(DataSetInformationType::getName).map(NameType::getTreatmentStandardsRoutes)
				.map(LifeCycleModelDataSet::adaptMultiLang).orElse(new HashMap<>());
	}

	public Map<String, String> safeGetMixAndLocationTypes() {

		return optionalDSItype().map(DataSetInformationType::getName).map(NameType::getMixAndLocationTypes)
				.map(LifeCycleModelDataSet::adaptMultiLang).orElse(new HashMap<>());
	}

	public Map<String, String> safeGetFunctionalUnitFlowProperties() {

		return optionalDSItype().map(DataSetInformationType::getName).map(NameType::getFunctionalUnitFlowProperties)
				.map(LifeCycleModelDataSet::adaptMultiLang).orElse(new HashMap<>());
	}

	public List<ClassificationType> safeGetClassificationInformation() {
		return optionalDSItype().map(DataSetInformationType::getClassificationInformation)
				.map(ClassificationInformationType::getClassification).orElse(new ArrayList<>());
	}

	public List<GlobalReferenceType> safeGetReferenceToResultingProcess() {
		return optionalDSItype().map(DataSetInformationType::getReferenceToResultingProcess).orElse(new ArrayList<>());
	}

	public Map<String, String> safeGetGeneralComment() {
		return optionalDSItype().map(DataSetInformationType::getGeneralComment)
				.map(LifeCycleModelDataSet::adaptMultiLang).orElse(new HashMap<>());
	}

	public List<GlobalReferenceType> safeGetReferenceToExternalDocumentation() {
		return optionalDSItype().map(DataSetInformationType::getReferenceToExternalDocumentation)
				.orElse(new ArrayList<>());
	}

	// === Quantitative Reference === //

	private Optional<QuantitativeReferenceType> optionalQuantitativeReference() {
		return Optional.ofNullable(this).map(LifeCycleModelDataSet::getLifeCycleModelInformation)
				.map(LifeCycleModelInformationType::getQuantitativeReference);
	}

	public BigInteger getNullableReferenceToReferenceProcess() {
		return optionalQuantitativeReference().map(QuantitativeReferenceType::getReferenceToReferenceProcess)
				.orElse(null);
	}

	// === Technology === //

	private Optional<TechnologyType> optionalTechnologyGroup() {
		return Optional.ofNullable(this).map(LifeCycleModelDataSet::getLifeCycleModelInformation)
				.map(LifeCycleModelInformationType::getTechnology);
	}

	public List<GroupType> safeGetGroups() {
		return optionalTechnologyGroup().map(TechnologyType::getGroupDeclarations).map(GroupDeclarationsType::getGroup)
				.orElse(new ArrayList<>());
	}

	public List<ProcessInstanceType> safeGetProcessInstance() {
		return optionalTechnologyGroup().map(TechnologyType::getProcesses).map(ProcessesType::getProcessInstance)
				.orElse(new ArrayList<>());
	}

	public List<GlobalReferenceType> safeGetReferenceToDiagram() {
		return optionalTechnologyGroup().map(TechnologyType::getReferenceToDiagram).orElse(new ArrayList<>());
	}

	// === ModellingAndValidation === //

	private Optional<ModellingAndValidationType> optionalModellingAndValidation() {
		return Optional.ofNullable(this).map(LifeCycleModelDataSet::getModellingAndValidation);
	}

	public Map<String, String> safeGetUseAdviceForDataSet() {
		return optionalModellingAndValidation().map(ModellingAndValidationType::getDataSourcesTreatmentEtc)
				.map(DataSourcesTreatmentEtcType::getUseAdviceForDataSet).map(LifeCycleModelDataSet::adaptMultiLang)
				.orElse(new HashMap<>());
	}

	public List<ReviewType> safeGetReviews() {
		return optionalModellingAndValidation().map(ModellingAndValidationType::getValidation)
				.map(ValidationType::getReview).orElse(new ArrayList<>());
	}

	public List<ComplianceDeclarationsType> safeGetComplianceDeclarations() {
		return optionalModellingAndValidation().map(ModellingAndValidationType::getComplianceDeclarations)
				.orElse(new ArrayList<>());
	}

	// === AdministrativeInformation === //

	public AdministrativeInformationType getNullableAdministrativeInformation() {
		return Optional.ofNullable(this).map(LifeCycleModelDataSet::getAdministrativeInformation).orElse(null);
	}

	private static Map<String, String> adaptMultiLang(FTMultiLang s) {
		Map<String, String> erd = new HashMap<String, String>();
		if (s != null)
			erd.put(s.getLang(), s.getValue());
		return erd;
	}

	private static Map<String, String> adaptMultiLang(List<StringMultiLang> ls) {
		Map<String, String> erd = new HashMap<String, String>();
		if (ls != null)
			for (StringMultiLang s : ls)
				erd.put(s.getLang(), s.getValue());
		return erd;
	}

}