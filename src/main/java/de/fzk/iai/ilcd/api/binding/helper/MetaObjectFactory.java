package de.fzk.iai.ilcd.api.binding.helper;

import de.fzk.iai.ilcd.api.dataset.ILCDTypes;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a factory for ObjectFactories.
 * 
 */
public class MetaObjectFactory {

	private static MetaObjectFactory _instance = null;

	private Map<ILCDTypes, Object> factories = null;

	/**
	 * Get the instance of this class.
	 * 
	 * @return the instance
	 */
	public static MetaObjectFactory getInstance() {
		if (_instance == null) {
			_instance = new MetaObjectFactory();
		}
		return _instance;
	}

	public MetaObjectFactory() {
		this.factories = new HashMap<ILCDTypes, Object>();
	}

	/**
	 * Get the suitable ObjectFactory for the specified type.
	 * 
	 * @param type
	 *            the type
	 * @return the ObjectFactory
	 */
	public Object getObjectFactory(ILCDTypes type) {

		if (!this.factories.containsKey(type)) {
			Object factory = null;

			switch (type) {
			case CONTACT:
				factory = new de.fzk.iai.ilcd.api.binding.generated.contact.ObjectFactory();
				break;
			case FLOW:
				factory = new de.fzk.iai.ilcd.api.binding.generated.flow.ObjectFactory();
				break;
			case FLOWPROPERTY:
				factory = new de.fzk.iai.ilcd.api.binding.generated.flowproperty.ObjectFactory();
				break;
			case LCIAMETHOD:
				factory = new de.fzk.iai.ilcd.api.binding.generated.lciamethod.ObjectFactory();
				break;
			case PROCESS:
				factory = new de.fzk.iai.ilcd.api.binding.generated.process.ObjectFactory();
				break;
			case SOURCE:
				factory = new de.fzk.iai.ilcd.api.binding.generated.source.ObjectFactory();
				break;
			case UNITGROUP:
				factory = new de.fzk.iai.ilcd.api.binding.generated.unitgroup.ObjectFactory();
				break;
			case COMMON:
				factory = new de.fzk.iai.ilcd.api.binding.generated.common.ObjectFactory();
				break;
			case CATEGORIES:
				factory = new de.fzk.iai.ilcd.api.binding.generated.categories.ObjectFactory();
				break;
			case LOCATIONS:
				factory = new de.fzk.iai.ilcd.api.binding.generated.locations.ObjectFactory();
				break;
			case LCIAMETHODOLOGIES:
				factory = new de.fzk.iai.ilcd.api.binding.generated.lciamethodologies.ObjectFactory();
				break;
			case LIFECYCLEMODEL:
				factory = new de.fzk.iai.ilcd.api.binding.generated.lifecyclemodel.ObjectFactory();
				break;
			}
			this.factories.put(type, factory);
			return factory;
		}
		return this.factories.get(type);
	}
}
