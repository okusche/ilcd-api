//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-34 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.03.01 at 01:55:00 PM MEZ 
//

package de.fzk.iai.ilcd.api.binding.generated.flowproperty;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;
import de.fzk.iai.ilcd.api.app.flowproperty.AdministrativeInformation;
import de.fzk.iai.ilcd.api.app.flowproperty.FlowPropertiesInformation;
import de.fzk.iai.ilcd.api.app.flowproperty.ModellingAndValidation;
import de.fzk.iai.ilcd.api.binding.generated.common.Other;
import de.fzk.iai.ilcd.api.dataset.DataSet;

/**
 * <p>
 * Java class for FlowPropertyDataSetType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="FlowPropertyDataSetType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="flowPropertiesInformation" type="{http://lca.jrc.it/ILCD/FlowProperty}FlowPropertiesInformationType"/&gt;
 *         &lt;element name="modellingAndValidation" type="{http://lca.jrc.it/ILCD/FlowProperty}ModellingAndValidationType" minOccurs="0"/&gt;
 *         &lt;element name="administrativeInformation" type="{http://lca.jrc.it/ILCD/FlowProperty}AdministrativeInformationType" minOccurs="0"/&gt;
 *         &lt;element ref="{http://lca.jrc.it/ILCD/Common}other" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="version" use="required" type="{http://lca.jrc.it/ILCD/Common}SchemaVersion" /&gt;
 *       &lt;anyAttribute processContents='lax' namespace='##other'/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlowPropertyDataSetType", propOrder = { "flowPropertiesInformation", "modellingAndValidation", "administrativeInformation", "other" })
public class FlowPropertyDataSetType extends DataSet {

	@XmlElement(required = true, type = FlowPropertiesInformation.class)
	protected FlowPropertiesInformation flowPropertiesInformation;

	@XmlElement(type = ModellingAndValidation.class)
	protected ModellingAndValidation modellingAndValidation;

	@XmlElement(type = AdministrativeInformation.class)
	protected AdministrativeInformation administrativeInformation;

	@XmlElement(namespace = "http://lca.jrc.it/ILCD/Common")
	protected Other other;

	@XmlAttribute(required = true)
	protected String version = "1.1";

	@XmlAnyAttribute
	private Map<QName, String> otherAttributes = new HashMap<QName, String>();

	/**
	 * Gets the value of the flowPropertiesInformation property.
	 * 
	 * @return possible object is {@link FlowPropertiesInformationType }
	 * 
	 */
	public FlowPropertiesInformationType getFlowPropertiesInformation() {
		return flowPropertiesInformation;
	}

	/**
	 * Sets the value of the flowPropertiesInformation property.
	 * 
	 * @param value
	 *            allowed object is {@link FlowPropertiesInformationType }
	 * 
	 */
	public void setFlowPropertiesInformation(FlowPropertiesInformationType value) {
		this.flowPropertiesInformation = ((FlowPropertiesInformation) value);
	}

	/**
	 * Gets the value of the modellingAndValidation property.
	 * 
	 * @return possible object is {@link ModellingAndValidationType }
	 * 
	 */
	public ModellingAndValidationType getModellingAndValidation() {
		return modellingAndValidation;
	}

	/**
	 * Sets the value of the modellingAndValidation property.
	 * 
	 * @param value
	 *            allowed object is {@link ModellingAndValidationType }
	 * 
	 */
	public void setModellingAndValidation(ModellingAndValidationType value) {
		this.modellingAndValidation = ((ModellingAndValidation) value);
	}

	/**
	 * Gets the value of the administrativeInformation property.
	 * 
	 * @return possible object is {@link AdministrativeInformationType }
	 * 
	 */
	public AdministrativeInformationType getAdministrativeInformation() {
		return administrativeInformation;
	}

	/**
	 * Sets the value of the administrativeInformation property.
	 * 
	 * @param value
	 *            allowed object is {@link AdministrativeInformationType }
	 * 
	 */
	public void setAdministrativeInformation(AdministrativeInformationType value) {
		this.administrativeInformation = ((AdministrativeInformation) value);
	}

	/**
	 * Gets the value of the other property.
	 * 
	 * @return possible object is {@link Other }
	 * 
	 */
	public Other getOther() {
		return other;
	}

	/**
	 * Sets the value of the other property.
	 * 
	 * @param value
	 *            allowed object is {@link Other }
	 * 
	 */
	public void setOther(Other value) {
		this.other = value;
	}

	/**
	 * Gets the value of the version property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the value of the version property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVersion(String value) {
		this.version = value;
	}

	/**
	 * Gets a map that contains attributes that aren't bound to any typed
	 * property on this class.
	 * 
	 * <p>
	 * the map is keyed by the name of the attribute and the value is the string
	 * value of the attribute.
	 * 
	 * the map returned by this method is live, and you can add new attribute by
	 * updating the map directly. Because of this design, there's no setter.
	 * 
	 * 
	 * @return always non-null
	 */
	public Map<QName, String> getOtherAttributes() {
		return otherAttributes;
	}

}
