package de.fzk.iai.ilcd.api.binding.helper;

import de.fzk.iai.ilcd.api.app.categories.CategorySystem;
import de.fzk.iai.ilcd.api.binding.generated.categories.CategorySystemType;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.output.DOMOutputter;

import javax.xml.bind.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public class CategoriesHelper extends AbstractHelper {

	/**
	 * Marshal an object graph representation of a categories file to an XML outputstream
	 * 
	 * @param catSystem
	 *            the categories system
	 * @param outputStream
	 *            the outputstream to write to
	 * @throws JAXBException
	 */
	public void marshal( CategorySystem catSystem, OutputStream outputStream ) throws JAXBException {

		Marshaller marshaller = createMarshaller( ILCDTypes.CATEGORIES );

		marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );

		// marshaller.setProperty("com.sun.xml.bind.xmlHeaders",
		// "<?xml-stylesheet type='text/xsl' href='../../stylesheets/" + type.getStylesheetName() + "'?>");

		try {
			marshal( catSystem, marshaller, outputStream );
		}
		catch ( Exception e ) {
			e.printStackTrace();
		}
		finally {
		}
	}

	protected void marshal( CategorySystem catSystem, Marshaller marshaller, OutputStream outputStream ) throws JAXBException {
		JAXBElement<CategorySystemType> poElement = createJAXBElement( catSystem );
		marshaller.marshal( poElement, outputStream );
	}
	
	/**
	 * Unmarshal an XML input stream to an object graph representation.
	 * 
	 * @param inputStream
	 *            the input stream
	 * @return the dataset as an object graph
	 * @throws JAXBException
	 * @throws UnsupportedEncodingException
	 */
	@SuppressWarnings("unchecked")
	public CategorySystem unmarshal(InputStream inputStream) throws JAXBException,
			UnsupportedEncodingException {

		JAXBContext jc = ContextFactory.getInstance().getContext(ILCDTypes.CATEGORIES);

		Unmarshaller unmarshaller = jc.createUnmarshaller();

		unmarshaller.setProperty("com.sun.xml.bind.ObjectFactory",
				MetaObjectFactory.getInstance().getObjectFactory(ILCDTypes.CATEGORIES));

		JAXBElement<Object> poe = (JAXBElement<Object>) unmarshaller.unmarshal(wrapInputStream(inputStream));

		log.debug(poe.getValue().getClass().getName());

		return (CategorySystem) poe.getValue();

	}


	/**
	 * Unmarshal a document to an object graph representation.
	 * 
	 * @param doc
	 *            the document
	 * @return the dataset as an object graph
	 * @throws JAXBException
	 */
	@SuppressWarnings("unchecked")
	public CategorySystem unmarshal(Document doc) throws JAXBException {

		CategorySystem result = null;

		JAXBContext jc = ContextFactory.getInstance().getContext(ILCDTypes.CATEGORIES);

		Unmarshaller unmarshaller = jc.createUnmarshaller();

		unmarshaller.setProperty("com.sun.xml.bind.ObjectFactory",
				MetaObjectFactory.getInstance().getObjectFactory(ILCDTypes.CATEGORIES));

		JAXBElement<Object> poe;

		try {
			poe = (JAXBElement<Object>) unmarshaller.unmarshal((new DOMOutputter()).output(doc));
			log.debug(poe.getValue().getClass().getName());
			result = (CategorySystem) poe.getValue();
		} catch (JDOMException e) {
			e.printStackTrace();
		}

		return result;
	}

	protected JAXBElement<CategorySystemType> createJAXBElement( CategorySystem catSystem ) {
		return (JAXBElement<CategorySystemType>) ((de.fzk.iai.ilcd.api.binding.generated.categories.ObjectFactory) MetaObjectFactory.getInstance()
				.getObjectFactory( ILCDTypes.CATEGORIES )).createCategorySystem( catSystem );
	}

}
