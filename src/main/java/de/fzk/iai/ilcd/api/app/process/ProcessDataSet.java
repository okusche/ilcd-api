package de.fzk.iai.ilcd.api.app.process;

import de.fzk.iai.ilcd.api.binding.generated.common.Other;
import de.fzk.iai.ilcd.api.binding.generated.process.ProcessInformationType;
import de.fzk.iai.ilcd.api.extension.productmodel.ProductModel;

import java.util.List;
import java.util.Optional;

public class ProcessDataSet extends de.fzk.iai.ilcd.api.binding.generated.process.ProcessDataSetType {

	public ProductModel getProductModel() {

		List<Object> lo = Optional.ofNullable(this)
				.map(de.fzk.iai.ilcd.api.binding.generated.process.ProcessDataSetType::getProcessInformation)
				.map(ProcessInformationType::getOther).map(Other::getAny).orElse(null);
		if (lo != null)
			for (Object o : lo)
				if (o instanceof ProductModel) {
					return (ProductModel) o;
				}
		return null;
	}

}
