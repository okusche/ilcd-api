package de.fzk.iai.ilcd.api.binding.helper;

import de.fzk.iai.ilcd.api.Configuration;
import de.fzk.iai.ilcd.api.Constants;
import de.fzk.iai.ilcd.api.app.common.FlowCategorization;
import de.fzk.iai.ilcd.api.app.common.GlobalReference;
import de.fzk.iai.ilcd.api.app.common.MultiLangStringList;
import de.fzk.iai.ilcd.api.app.common.STMultiLang;
import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.api.app.flow.FlowDataSet;
import de.fzk.iai.ilcd.api.app.flowproperty.FlowPropertyDataSet;
import de.fzk.iai.ilcd.api.app.lciamethod.LCIAMethodDataSet;
import de.fzk.iai.ilcd.api.app.lifecyclemodel.LifeCycleModelDataSet;
import de.fzk.iai.ilcd.api.app.process.ProcessDataSet;
import de.fzk.iai.ilcd.api.app.source.SourceDataSet;
import de.fzk.iai.ilcd.api.app.unitgroup.UnitGroupDataSet;
import de.fzk.iai.ilcd.api.binding.generated.common.FlowTypeValues;
import de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceType;
import de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceTypeValues;
import de.fzk.iai.ilcd.api.dataset.DataSet;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.fzk.iai.ilcd.api.util.TreeWalker;
import de.schlichtherle.io.FileInputStream;
import de.schlichtherle.io.FileOutputStream;
import org.apache.logging.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.transform.JDOMResult;

import javax.xml.bind.UnmarshalException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.URIResolver;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

/**
 * Provides some basic methods for reading/writing ILCD datasets from/to the
 * file system or network.
 * 
 */
public class DatasetDAO {
	protected final Logger log = org.apache.logging.log4j.LogManager.getLogger(DatasetDAO.class);

	private DatasetHelper datasetHelper = null;

	/**
	 * Sole constructor.
	 */
	public DatasetDAO() {
		this.datasetHelper = new DatasetHelper();
	}

	/**
	 * Constructor specifiying a URIResolver to be used by the underlying
	 * DatasetHelper class.
	 * 
	 * @param resolver
	 *            the resolver
	 */
	public DatasetDAO(URIResolver resolver) {
		this.datasetHelper = new DatasetHelper(resolver);
	}

	/**
	 * Checks if a DOM document contains a valid ILCD 1.1 dataset
	 * 
	 * @param doc
	 *            the org.jdom2.Document
	 * @return true if the document contains an ILCD 1.1 dataset
	 */
	public boolean isILCD(org.jdom2.Document doc) {
		String version = doc.getRootElement().getAttribute("version").getValue();
		String namespace = doc.getRootElement().getNamespaceURI();
		String rootElement = doc.getRootElement().getName();
		return isILCD(version, namespace, rootElement);
	}

	/**
	 * Checks if a DOM document contains a valid ILCD 1.1 dataset
	 * 
	 * @param doc
	 *            the org.dom4j.Document
	 * @return true if the document contains an ILCD 1.1 dataset
	 */
	public boolean isILCD(org.dom4j.Document doc) {
		String version = doc.getRootElement().attribute("version") != null ? doc.getRootElement().attribute("version").getValue() : null;
		String namespace = doc.getRootElement().getNamespaceURI();
		String rootElement = doc.getRootElement().getName();
		return isILCD(version, namespace, rootElement);
	}

	/**
	 * Checks if the given parameters comply with the ILCD 1.1 specification:
	 * 
	 * version must be 1.1, namespace URI and root element name must be one of
	 * those specified for ILCD 1.1
	 * 
	 * @param version
	 *            the format version number
	 * @param namespace
	 *            the namespace URI
	 * @param rootElement
	 *            the name of the root element
	 * @return true if all the criteria match the ILCD 1.1 specification
	 */
	public boolean isILCD(String version, String namespace, String rootElement) {
		if (version == null || namespace == null || rootElement == null)
			return false;
		return (version.equals("1.1")
				&& (namespace.equals(Constants.NS_PROCESS) || namespace.equals(Constants.NS_FLOW) || namespace.equals(Constants.NS_FLOWPROPERTY)
						|| namespace.equals(Constants.NS_UNITGROUP) || namespace.equals(Constants.NS_LCIAMETHOD) || namespace.equals(Constants.NS_CONTACT)
						|| namespace.equals(Constants.NS_SOURCE) || namespace.equals(Constants.NS_LIFECYCLEMODEL)) && (rootElement.equals(Constants.PROCESS_ROOT_ELEMENT_NAME)
				|| rootElement.equals(Constants.FLOW_ROOT_ELEMENT_NAME) || rootElement.equals(Constants.FLOW_PROPERTY_ROOT_ELEMENT_NAME)
				|| rootElement.equals(Constants.UNIT_GROUP_ROOT_ELEMENT_NAME) || rootElement.equals(Constants.LCIAMETHOD_ROOT_ELEMENT_NAME)
				|| rootElement.equals(Constants.CONTACT_ROOT_ELEMENT_NAME) || rootElement.equals(Constants.SOURCE_ROOT_ELEMENT_NAME) || rootElement.equals(Constants.LIFECYCLEMODEL_ROOT_ELEMENT_NAME)));
	}

	private String getVersion(Document doc) {
		String result = null;

		result = doc.getRootElement().getAttributeValue("version");
		log.debug("version is " + result);

		return result;
	}

	/**
	 * migrate dataset from the previous format version ELCD 1.0
	 * 
	 * @param doc
	 */
	protected Document migrateDataset(Document doc) {

		log.info("migrating dataset");

		try {
			JDOMResult result = new JDOMResult();

			List<String> messages = this.datasetHelper.xsltTransform(doc, Constants.MIGRATE_1_1_STYLESHEET_NAME, result);

			log.info(messages);

			return result.getDocument();

		} catch (RuntimeException e) {
			log.error("unknown foreign format -", e);
		} catch (Exception e) {
			log.error("unknown foreign format", e);
		}
		return null;
	}

	/**
	 * Reads a dataset from the file system
	 * 
	 * @param fileName
	 *            the name of the file to read
	 * @return the dataset
	 */
	public DataSet openDataset(String fileName) {

		log.debug("trying to open file " + fileName);

		Document doc = null;
		try {
			doc = this.datasetHelper.parse(new FileInputStream(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return openDataset(doc);

	}
	
	/**
	 * Reads a dataset from an input stream
	 * 
	 * @param inputStream
	 *            the InputStream to read from
	 * @return the dataset
	 */
	public DataSet openDataset(InputStream inputStream) {
		Document doc = null;
		doc = this.datasetHelper.parse(inputStream);
		return openDataset(doc);
	}

	/**
	 * Reads a dataset from a URL
	 * 
	 * @param url
	 *            the URL
	 * @return the dataset
	 */
	public DataSet openDataset(URL url) throws FileNotFoundException {

		Document doc = this.datasetHelper.parse(completeUrl(url));
		return openDataset(doc);
	}

	/**
	 * Transforms a DOM document into a Dataset object
	 * 
	 * @param doc
	 * @return the dataset
	 */
	private DataSet openDataset(Document doc) {

		String version = getVersion(doc);
		DataSet result = null;
		if (version == null)
			return null;
		else if (version.equals("1.1")) {
			ILCDTypes type = determineDatasetType(doc);
			try {
				result = openDataset(type, doc);
			} catch (Exception e) {
				e.printStackTrace();
				result = openDataset(type, doc);
			}
		} else if (version.equals("1.0")) {
			DataSet dataset = openDataset(migrateDataset(doc));
			dataset.setMigratedFrom("1.0");
			return dataset;
		} else {
			log.error("Unknown format version");
			return null;
		}
		return result;
	}

	/**
	 * Transforms a DOM document into a Dataset object
	 * 
	 * @param type
	 *            the type of the dataset
	 * @param doc
	 *            the DOM document
	 * @return the dataset
	 */
	public DataSet openDataset(ILCDTypes type, Document doc) {
		DataSet dataset = null;

		try {
			ContextFactory.getInstance().getContext(type);
			MetaObjectFactory.getInstance().getObjectFactory(type);

			dataset = this.datasetHelper.unmarshal(doc, type);

		} catch (UnmarshalException e) {
			log.error("Error opening dataset - unknown format", e);
		} catch (Exception e) {
			log.error("Error opening dataset", e);
		}
		return dataset;

	}

	/**
	 * Writes a dataset to disk. The dataset's time stamp will be updated to the
	 * current date and time prior to writing.
	 * 
	 * @param dataset
	 * @param fileName
	 */
	public void saveDataset(DataSet dataset, String fileName) {
		saveDataset(dataset, fileName, true);
	}

	/**
	 * Writes a dataset to disk, specifiying whether to update the dataset's
	 * time stamp or not.
	 * 
	 * @param dataset
	 * @param fileName
	 * @param updateTimeStamp
	 */
	protected void saveDataset(DataSet dataset, String fileName, boolean updateTimeStamp) {

		OutputStream out = null;
		try {

			if (updateTimeStamp)
				updateTimeStamp(dataset);

			out = new FileOutputStream(fileName);

			this.datasetHelper.marshal(dataset, out);

		} catch (Exception e) {
			log.error("Error saving dataset", e);
		} finally {
			try {
				out.close();
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Updates a dataset's time stamp to the current date and time.
	 * 
	 * @param dataset to update
	 */
	public void updateTimeStamp(DataSet dataset) {

		try {
			XMLGregorianCalendar xcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar());

			// make sure this section is initialized properly
			TreeWalker.doWalk(dataset.getDatasetType().getRootElementName() + "-administrativeInformation-dataEntryBy-timeStamp", dataset, null);

			Method getAdministrativeInformation = dataset.getClass().getMethod("getAdministrativeInformation", (Class[]) null);

			Object administrativeInformation = getAdministrativeInformation.invoke(dataset, (Object[]) null);

			Method getDataEntryBy = administrativeInformation.getClass().getMethod("getDataEntryBy", (Class[]) null);

			Object dataEntryBy = getDataEntryBy.invoke(administrativeInformation, (Object[]) null);

			Method setTimeStamp = dataEntryBy.getClass().getMethod("setTimeStamp", XMLGregorianCalendar.class);

			setTimeStamp.invoke(dataEntryBy, xcal);
		} catch (Exception e) {
			log.error("Error setting timestamp", e);
		}
	}

	/**
	 * Sets a dataset's dataset format to the default one specified in
	 * {@link de.fzk.iai.ilcd.api.Constants}.
	 * 
	 * @param dataset
	 */
	@SuppressWarnings("unchecked")
	public void setDatasetFormat(DataSet dataset) {
		try {
			// make sure this section is initialized properly
			TreeWalker.doWalk(dataset.getDatasetType().getRootElementName() + "-administrativeInformation-dataEntryBy", dataset, null);

			Method getAdministrativeInformation = dataset.getClass().getMethod("getAdministrativeInformation", (Class[]) null);

			Object administrativeInformation = getAdministrativeInformation.invoke(dataset, (Object[]) null);

			Method getDataEntryBy = administrativeInformation.getClass().getMethod("getDataEntryBy", (Class[]) null);

			Object dataEntryBy = getDataEntryBy.invoke(administrativeInformation, (Object[]) null);

			GlobalReferenceType formatRef = new GlobalReferenceType();

			formatRef.setType(GlobalReferenceTypeValues.SOURCE_DATA_SET);
			formatRef.setRefObjectId(Constants.DATASET_FORMAT_SOURCE_UUID);
			formatRef.setUri(Constants.DATASET_FORMAT_SOURCE_PATH);
			// formatRef.setVersion(Constants.DATASET_FORMAT_SOURCE_VERSION);

			STMultiLang sd = new STMultiLang();
			sd.setLang(Configuration.getInstance().getLanguage());
			sd.setValue("ILCD format"); // + Constants.FORMAT_VERSION
			formatRef.getShortDescription().add(sd);

			Method getReferenceToDataSetFormat = dataEntryBy.getClass().getMethod("getReferenceToDataSetFormat", (Class[]) null);

			List<GlobalReferenceType> references = (List<GlobalReferenceType>) getReferenceToDataSetFormat.invoke(dataEntryBy, (Object[]) null);
			references.add(formatRef);

		} catch (Exception e) {
			log.error("Error setting dataset format", e);
		}
	}

	/**
	 * Sets a dataset's version to 01.00.000
	 * 
	 * @param dataset
	 */
	public void setDatasetVersion(DataSet dataset) {
		setDatasetVersion(dataset, "01.00.000");
	}

	/**
	 * increment a dataset's minor version
	 * 
	 * @param dataset
	 */
	public void incrementDatasetVersion(DataSet dataset) {
		String newVersion = incrementVersion(getVersion(dataset));
		setDatasetVersion(dataset, newVersion);
	}

	/**
	 * increment the minor version in a version string
	 * 
	 * @param version string
	 * @return the version string with incremented minor version
	 */
	public String incrementVersion(String version) {

		int majorSeparatorPos = version.indexOf(".");
		int minorSeparatorPos = version.indexOf(".", majorSeparatorPos + 1);

		String major = version.substring(0, majorSeparatorPos);

		String minor = version.substring(majorSeparatorPos + 1, minorSeparatorPos);

		String revision = version.substring(minorSeparatorPos + 1, version.length());

		int intMinor = Integer.parseInt(minor);
		intMinor++;

		return major + "." + String.format("%02d", intMinor) + "." + revision;
	}

	/**
	 * Sets a dataset's version
	 * 
	 * @param dataset
	 */
	public void setDatasetVersion(DataSet dataset, String version) {
		try {
			// make sure this section is initialized properly
			TreeWalker.doWalk(dataset.getDatasetType().getRootElementName() + "-administrativeInformation-publicationAndOwnership", dataset, null);

			Method getAdministrativeInformation = dataset.getClass().getMethod("getAdministrativeInformation", (Class[]) null);

			Object administrativeInformation = getAdministrativeInformation.invoke(dataset, (Object[]) null);

			Method getPublicationAndOwnership = administrativeInformation.getClass().getMethod("getPublicationAndOwnership", (Class[]) null);

			Object publicationAndOwnership = getPublicationAndOwnership.invoke(administrativeInformation, (Object[]) null);

			Method setDataSetVersion = publicationAndOwnership.getClass().getMethod("setDataSetVersion", String.class);

			setDataSetVersion.invoke(publicationAndOwnership, version);
		} catch (Exception e) {
			log.error("Error setting dataset version", e);
		}
	}

	/**
	 * Sets a dataset's UUID to a newly generated one.
	 * 
	 * @param dataset whose UUID is to be set
	 */
	public void setUUID(DataSet dataset) {
		try {
			ILCDTypes dsType = dataset.getDatasetType();

			// "sourceInformation", for example
			String xxxInformationId = (dsType.getRootElementName().equals("flowPropertyDataSet") ? "flowPropertiesInformation" : dsType.getRootElementName()
					.substring(0, dsType.getRootElementName().indexOf("DataSet")) + "Information");

			xxxInformationId = xxxInformationId.substring(0, 1).toUpperCase() + xxxInformationId.substring(1, xxxInformationId.length());

			// make sure this section is initialized properly
			TreeWalker.doWalk(dsType.getRootElementName() + "-" + xxxInformationId + "-dataSetInformation", dataset, null);

			Method getXXXInformation = dataset.getClass().getMethod("get" + xxxInformationId, (Class[]) null);

			Object xxxInformation = getXXXInformation.invoke(dataset, (Object[]) null);

			Method getDataSetInformation = xxxInformation.getClass().getMethod("getDataSetInformation", (Class[]) null);

			Object dataSetInformation = getDataSetInformation.invoke(xxxInformation, (Object[]) null);

			Method setUUID = dataSetInformation.getClass().getMethod("setUUID", String.class);

			UUID uuid = UUID.randomUUID();

			log.debug("generated new UUID " + uuid);

			setUUID.invoke(dataSetInformation, uuid.toString());

		} catch (Exception e) {
			log.error("Error setting UUID", e);
		}
	}

	/**
	 * Get a dataset's UUID.
	 * 
	 */
	public String getUUID(DataSet dataset) {
		ILCDTypes dsType = dataset.getDatasetType();

		// "sourceInformation", for example
		String xxxInformationId = (dsType.getRootElementName().equals("flowPropertyDataSet") ? "flowPropertiesInformation" : dsType.getRootElementName()
				.substring(0, dsType.getRootElementName().indexOf("DataSet")) + "Information");

		xxxInformationId = xxxInformationId.substring(0, 1).toUpperCase() + xxxInformationId.substring(1, xxxInformationId.length());

		String xPath = dsType.getRootElementName() + "-" + xxxInformationId + "-dataSetInformation-UUID";

		TreeWalker walker = new TreeWalker(xPath, dsType);

		return (String) walker.getValue(dataset);

	}

	/**
	 * Get a dataset's version.
	 * 
	 */
	public String getVersion(DataSet dataset) {
		ILCDTypes dsType = dataset.getDatasetType();

		String xPath = dsType.getRootElementName() + "-administrativeInformation-publicationAndOwnership-dataSetVersion";

		TreeWalker walker = new TreeWalker(xPath, dsType);

		return (String) walker.getValue(dataset);

	}

	/**
	 * Get a dataset's permanentURI.
	 * 
	 */
	public String getPermanentURI(DataSet dataset) {
		ILCDTypes dsType = dataset.getDatasetType();

		String xPath = dsType.getRootElementName() + "-administrativeInformation-publicationAndOwnership-permanentDataSetURI";

		TreeWalker walker = new TreeWalker(xPath, dsType);

		return (String) walker.getValue(dataset);

	}

	/**
	 * Determines the type of a dataset represented by a DOM document.
	 * 
	 * @param doc
	 *            the DOM document
	 * @return the type
	 */
	public ILCDTypes determineDatasetType(Document doc) {

		String rootElementName = doc.getRootElement().getName();

		log.debug("Root element name is " + rootElementName);

		ILCDTypes result = ILCDTypes.determineType(rootElementName);

		if (result == null)
			log.error("XML ROOT ELEMENT " + rootElementName + " IS NOT SUPPORTED!");

		return result;
	}

	/**
	 * Determines the type of a dataset.
	 * 
	 * @param fileName
	 *            the file name of the dataset to inspect
	 * @return the type
	 */
	public ILCDTypes determineDatasetType(String fileName) {

		try {
			log.debug("determining dataset type for file " + fileName);

			return determineDatasetType(new FileInputStream(fileName));

		} catch (IOException e) {
			log.error(e);
		} finally {
		}

		return null;
	}

	/**
	 * Determines the type of a dataset.
	 * 
	 * @param stream
	 *            the InputStream of the dataset to inspect
	 * @return the type
	 */
	public ILCDTypes determineDatasetType(InputStream stream) {

		try {
			log.debug("determining dataset type for InputStream, " + stream.available() + " bytes available");

			SAXBuilder builder = new SAXBuilder();

			Document markup = builder.build(datasetHelper.wrapInputStream(stream));

			String rootElementName = markup.getRootElement().getName();

			log.debug("Root element name is " + rootElementName);

			ILCDTypes result = ILCDTypes.determineType(rootElementName);

			if (result == null)
				log.error("XML ROOT ELEMENT " + rootElementName + " IS NOT SUPPORTED!");

			return result;

		} catch (IOException e) {
			log.error(e);
		} catch (JDOMException e) {
			log.error(e);
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				log.error(e);
			}
		}

		return null;
	}

	/**
	 * Create a {@link GlobalReference} for a given file name. 
	 * 
	 * @param fileName
	 *            the file name
	 * @return the {@link GlobalReference}
	 */
	public GlobalReference createReference(String fileName) {
		DataSet dataset = this.openDataset(fileName);
		return createReference(dataset);
	}

	/**
	 * Create a {@link GlobalReference} for a given file name.
	 * 
	 * @param fileName
	 *            the file name to read the dataset from
	 * @param uri
	 *            the value to use for the URI field
	 * @return the {@link GlobalReference}
	 */
	public GlobalReference createReference(String fileName, String uri) {
		DataSet dataset = this.openDataset(fileName);
		return createReference(dataset, uri);
	}

	/**
	 * Create a {@link GlobalReference} for a given URL.
	 * 
	 * @param url
	 *            the URL
	 * @return the {@link GlobalReference}
	 * @throws FileNotFoundException
	 */
	public GlobalReference createReference(URL url) throws FileNotFoundException {
		DataSet dataset = this.openDataset(url);
		return createReference(dataset);
	}

	/**
	 * Create a {@link GlobalReference} for a given dataset.
	 * 
	 * The {@link GlobalReference}'s fields will be automatically populated with
	 * the values from the dataset.
	 * 
	 * @param dataset
	 *            the dataset
	 * @return the {@link GlobalReference}
	 */
	public GlobalReference createReference(DataSet dataset) {
		return createReference(dataset, null);

	}

	/**
	 * Create a {@link GlobalReference} for a given dataset.
	 * 
	 * The {@link GlobalReference}'s fields will be automatically populated with
	 * the values from the dataset.
	 * 
	 * @param dataset
	 *            the dataset
	 * @param uri
	 *            the value to use for the URI field
	 * @return the {@link GlobalReference}
	 */
	public GlobalReference createReference(DataSet dataset, String uri) {
		GlobalReference reference = new GlobalReference();
		reference.setType(GlobalReferenceTypeValues.fromValue(dataset.getDatasetTypeForReference()));
		reference.setRefObjectId(getUUID(dataset));
		if (uri == null)
			reference.setUri(getPermanentURI(dataset));
		else
			reference.setUri(uri);
		reference.setVersion(getVersion(dataset));

		String shortDesc = null;

		try {
			switch (dataset.getDatasetType()) {
			case PROCESS:
				shortDesc = ((MultiLangStringList<?>) ((ProcessDataSet) dataset).getProcessInformation().getDataSetInformation().getName().getBaseName())
						.getValue();
				break;
			case FLOW:
				FlowDataSet flow = (FlowDataSet) dataset;
				shortDesc = ((MultiLangStringList<?>) flow.getFlowInformation().getDataSetInformation().getName().getBaseName()).getValue();
				try {
					if (flow.getModellingAndValidation().getLCIMethod().getTypeOfDataSet().equals(FlowTypeValues.ELEMENTARY_FLOW)) {
						FlowCategorization flowCat = (FlowCategorization) flow.getFlowInformation().getDataSetInformation().getClassificationInformation()
								.getElementaryFlowCategorization().get(0);
						shortDesc += " (" + flowCat.getMostDetailedCategory().getValue() + ")";
					}
				} catch (NullPointerException e) {
					log.error(e);
				}
				break;
			case FLOWPROPERTY:
				shortDesc = ((MultiLangStringList<?>) ((FlowPropertyDataSet) dataset).getFlowPropertiesInformation().getDataSetInformation().getName())
						.getValue();
				break;
			case UNITGROUP:
				shortDesc = ((MultiLangStringList<?>) ((UnitGroupDataSet) dataset).getUnitGroupInformation().getDataSetInformation().getName()).getValue();
				break;
			case SOURCE:
				shortDesc = ((MultiLangStringList<?>) ((SourceDataSet) dataset).getSourceInformation().getDataSetInformation().getShortName()).getValue();
				break;
			case CONTACT:
				shortDesc = ((MultiLangStringList<?>) ((ContactDataSet) dataset).getContactInformation().getDataSetInformation().getName()).getValue();
				break;
			case LCIAMETHOD:
				shortDesc = ((MultiLangStringList<?>) ((LCIAMethodDataSet) dataset).getLCIAMethodInformation().getDataSetInformation().getName()).getValue();
				break;
			case LIFECYCLEMODEL: // only the desc. of basename
				shortDesc = ((MultiLangStringList<?>) ((LifeCycleModelDataSet) dataset).getLifeCycleModelInformation().getDataSetInformation().getName().getBaseName()).getValue();
				break;
			}
			reference.getShortDescription().add(new STMultiLang(shortDesc));
		} catch (Exception e) {
			log.error("could not extract shortDescription: ", e);
		}

		return reference;
	}

	/**
	 * Set the URIResolver of the underlying DatasetHelper class.
	 */
	public URIResolver getUriResolver() {
		return this.datasetHelper.getUriResolver();
	}

	/**
	 * Set the URIResolver of the underlying DatasetHelper class.
	 * 
	 * @param uriResolver
	 */
	public void setUriResolver(URIResolver uriResolver) {
		this.datasetHelper.setUriResolver(uriResolver);
	}

	/**
	 * Gets the dataset helper.
	 * 
	 * @return the dataset helper
	 */
	public DatasetHelper getDatasetHelper() {
		return datasetHelper;
	}

	/**
	 * Sets the dataset helper.
	 * 
	 * @param datasetHelper
	 *            the new dataset helper
	 */
	public void setDatasetHelper(DatasetHelper datasetHelper) {
		this.datasetHelper = datasetHelper;
	}

	/**
	 * This adds the necessary parameters to a URL pointing to a full dataset
	 * that request the provider to send raw xml
	 * 
	 * @param url
	 * @return
	 */
	public URL completeUrl(URL url) {
		try {
			if (!url.toString().endsWith("?format=xml&view=full"))
				return new URL(url.toString() + "?format=xml&view=full");
			else
				return url;
		} catch (MalformedURLException e) {
			log.warn(e);
		}
		return null;
	}

	public STMultiLang getShortDesc(GlobalReference ref) {

		return getShortDesc(ref, "");

	}

	public STMultiLang getShortDesc(GlobalReference ref, String pathPrefix) {

		String value = null;

		DataSet dataset = this.openDataset(pathPrefix + "/" + ref.getUri());

		value = dataset.getShortDesc();

		return new STMultiLang(value);

	}

	public void setShortDesc(GlobalReference ref) {

		STMultiLang shortDesc = getShortDesc(ref);

		ref.getShortDescription().add(shortDesc);

	}

	public void setShortDesc(GlobalReference ref, String pathPrefix) {

		STMultiLang shortDesc = getShortDesc(ref, pathPrefix);

		ref.getShortDescription().add(shortDesc);

	}

}
