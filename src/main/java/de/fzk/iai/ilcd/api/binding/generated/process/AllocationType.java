//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-34 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.03.01 at 01:55:00 PM MEZ 
//

package de.fzk.iai.ilcd.api.binding.generated.process;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AllocationType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="AllocationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="internalReferenceToCoProduct" type="{http://lca.jrc.it/ILCD/Common}Int6" /&gt;
 *       &lt;attribute name="allocatedFraction" type="{http://lca.jrc.it/ILCD/Common}Perc" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllocationType")
public class AllocationType {

	@XmlAttribute
	protected BigInteger internalReferenceToCoProduct;

	@XmlAttribute
	protected BigDecimal allocatedFraction;

	/**
	 * Gets the value of the internalReferenceToCoProduct property.
	 * 
	 * @return possible object is {@link BigInteger }
	 * 
	 */
	public BigInteger getInternalReferenceToCoProduct() {
		return internalReferenceToCoProduct;
	}

	/**
	 * Sets the value of the internalReferenceToCoProduct property.
	 * 
	 * @param value
	 *            allowed object is {@link BigInteger }
	 * 
	 */
	public void setInternalReferenceToCoProduct(BigInteger value) {
		this.internalReferenceToCoProduct = value;
	}

	/**
	 * Gets the value of the allocatedFraction property.
	 * 
	 * @return possible object is {@link BigDecimal }
	 * 
	 */
	public BigDecimal getAllocatedFraction() {
		return allocatedFraction;
	}

	/**
	 * Sets the value of the allocatedFraction property.
	 * 
	 * @param value
	 *            allowed object is {@link BigDecimal }
	 * 
	 */
	public void setAllocatedFraction(BigDecimal value) {
		this.allocatedFraction = value;
	}

}
