package de.fzk.iai.ilcd.api.app.common;

import de.fzk.iai.ilcd.api.binding.generated.common.CategoryType;

import java.util.ArrayList;
import java.util.List;

public class FlowCategorization extends de.fzk.iai.ilcd.api.binding.generated.common.FlowCategorizationType {
	/**
	 * 
	 */
	public FlowCategorization() {
	}

	/**
	 * Convenience constructor
	 * 
	 * @param cat
	 */
	public FlowCategorization(Category cat) {
		this.add(cat);
	}

	/**
	 * Convenience method to add a de.fzk.iai.ilcd.api.app.common.Category
	 * without calling the getCategory() method
	 * 
	 * @param cat
	 */
	public void add(Category cat) {
		this.getCategory().add(cat);
	}

	/**
	 * Remove the category that matches the level and value of the one passed as
	 * parameter
	 * 
	 * @param cat
	 */
	public void remove(Category cat) {
		int removalCandidateIndex = -1;

		for (CategoryType catType : this.getCategory()) {
			if (catType.getLevel().intValue() == cat.getLevel().intValue() && catType.getValue().equals(cat.getValue()))
				removalCandidateIndex = this.getCategory().indexOf(catType);
		}

		if (removalCandidateIndex != -1)
			this.getCategory().remove(removalCandidateIndex);
	}

	/**
	 * Remove all categories whose level is equal or greater than the parameter
	 * 
	 * @param level
	 */
	public void removeAll(int level) {
		List<CategoryType> removalCandidates = new ArrayList<CategoryType>();

		for (CategoryType catType : this.getCategory()) {
			if (catType.getLevel().intValue() >= level)
				removalCandidates.add(catType);
		}

		if (!removalCandidates.isEmpty())
			this.getCategory().removeAll(removalCandidates);
	}

	/**
	 * Convenience method to get the category with the highest level
	 * 
	 * @return the category
	 */
	public CategoryType getMostDetailedCategory() {
		CategoryType result = null;

		for (CategoryType catType : this.getCategory()) {
			if (result == null) {
				result = catType;
				continue;
			}
			if (catType.getLevel().intValue() > result.getLevel().intValue())
				result = catType;
		}

		return result;
	}

	/**
	 * Convenience method to get the number of present
	 * de.fzk.iai.ilcd.api.app.common.Category elements without calling the
	 * getCategory() method
	 * 
	 * @return the size
	 */
	public int size() {
		return this.getCategory().size();
	}

}
