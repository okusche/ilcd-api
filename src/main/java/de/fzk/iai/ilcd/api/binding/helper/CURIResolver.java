/**
 * 
 */
package de.fzk.iai.ilcd.api.binding.helper;

import de.fzk.iai.ilcd.api.Constants;
import de.fzk.iai.ilcd.api.ResourceAnchor;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;

public class CURIResolver implements URIResolver {

	protected static Logger log = org.apache.logging.log4j.LogManager.getLogger(CURIResolver.class);

	public CURIResolver(String file) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.xml.transform.URIResolver#resolve(java.lang.String,
	 * java.lang.String)
	 */
	public Source resolve(String href, String base) throws TransformerException {
		log.debug("trying to resolve href " + href + ", base " + base);
		String newHRef = "";

		try {
			ClassLoader cl = ResourceAnchor.class.getClassLoader();
			if (href.equalsIgnoreCase("../../stylesheets/common.xsl")) {
				newHRef = Constants.STYLESHEETS_PATH_PREFIX + "common.xsl";
				log.debug("returning " + newHRef);
				return new StreamSource(cl.getResourceAsStream(newHRef));
			} else if (href.startsWith("1.1_") || href.equalsIgnoreCase("namespace.xml") || href.equalsIgnoreCase("category_mappings_1.0.1_to_1.1.xml")) {
				newHRef = Constants.TOOLS_STYLESHEETS_PATH_PREFIX + href;
				log.debug("returning " + newHRef);
				return new StreamSource(cl.getResourceAsStream(newHRef));
			} else if (href.endsWith(".xsl") || href.endsWith("_Reference.xml") || href.endsWith("reference_types.xml")) {
				newHRef = Constants.STYLESHEETS_PATH_PREFIX + href;
				log.debug("returning " + newHRef);
				return new StreamSource(cl.getResourceAsStream(newHRef));
			} else if (href.endsWith("ILCDClassification.xml") || href.endsWith(Constants.DEFAULT_ILCD_LOCATIONS_NAME)) {
				newHRef = href.replaceAll("\\.\\.\\/", "");
				// if we're using different L&C, resolve these

				log.debug("returning " + newHRef);
				return new StreamSource(cl.getResource(newHRef).openStream());
			} else if (href.endsWith(Constants.COMPLIANCE_REFERENCE_CATEGORIES_FILE)) {
				newHRef = Constants.STYLESHEETS_PATH_PREFIX + href;
				log.debug("returning " + newHRef);
				return new StreamSource(cl.getResourceAsStream(Constants.COMPLIANCE_REFERENCE_CATEGORIES_PATH));
			} else if (href.contains("../schemas/") || href.contains("../sources/")) {
				newHRef = Constants.ILCD_PATH_PREFIX + href.replaceAll("\\.\\.\\/", "");
				log.debug("returning " + newHRef);
				return new StreamSource(cl.getResourceAsStream(newHRef));
			} else {
				log.debug("unknown href:" + href);
				return new StreamSource(href);
			}
		} catch (Exception e) {
			log.error(e);
			throw new TransformerException(e);
		}
	}
}