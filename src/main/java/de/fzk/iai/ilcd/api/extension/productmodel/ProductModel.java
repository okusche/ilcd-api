package de.fzk.iai.ilcd.api.extension.productmodel;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "productModel", namespace = "http://iai.kit.edu/ILCD/ProductModel")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "parameters", "nodes", "connections" })
public class ProductModel {

	@XmlAttribute
	protected String name;

	@XmlElementWrapper(name="parameters")
	@XmlElement(name = "parameter", namespace = "http://iai.kit.edu/ILCD/ProductModel", type = Parameter.class)
	protected List<Parameter> parameters = null;

	@XmlElementWrapper(name="nodes")
	@XmlElement(name = "process", namespace = "http://iai.kit.edu/ILCD/ProductModel", type = ProcessNode.class)
	protected List<ProcessNode> nodes = null;

	@XmlElementWrapper(name="connections")
	@XmlElement(name = "connector", namespace = "http://iai.kit.edu/ILCD/ProductModel", type = Connector.class)
	protected List<Connector> connections = null;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the nodes
	 */
	public List<ProcessNode> getNodes() {
		if (nodes == null)
			nodes = new ArrayList<ProcessNode>();
		return nodes;
	}

	/**
	 * @return the connections
	 */
	public List<Connector> getConnections() {
		if (connections == null)
			connections = new ArrayList<Connector>();
		return connections;
	}

	/**
	 * @return the parameters
	 */
	public List<Parameter> getParameters() {
		if (parameters == null)
			parameters = new ArrayList<Parameter>();
		return parameters;
	}

}
