package de.fzk.iai.ilcd.api.dataset;

import de.fzk.iai.ilcd.api.Constants;
import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.api.app.flow.FlowDataSet;
import de.fzk.iai.ilcd.api.app.flowproperty.FlowPropertyDataSet;
import de.fzk.iai.ilcd.api.app.lciamethod.LCIAMethodDataSet;
import de.fzk.iai.ilcd.api.app.lifecyclemodel.LifeCycleModelDataSet;
import de.fzk.iai.ilcd.api.app.process.ProcessDataSet;
import de.fzk.iai.ilcd.api.app.source.SourceDataSet;
import de.fzk.iai.ilcd.api.app.unitgroup.UnitGroupDataSet;

/**
 * @author oliver.kusche
 * 
 */
public enum ILCDTypes {

	CONTACT(Constants.PACKAGE_CONTACT), FLOW(Constants.PACKAGE_FLOW), FLOWPROPERTY(Constants.PACKAGE_FLOWPROPERTY),
	LCIAMETHOD(Constants.PACKAGE_LCIAMETHOD), PROCESS(Constants.PACKAGE_PROCESS), SOURCE(Constants.PACKAGE_SOURCE),
	UNITGROUP(Constants.PACKAGE_UNITGROUP), COMMON(Constants.PACKAGE_COMMON), CATEGORIES(Constants.PACKAGE_CATEGORIES),
	LOCATIONS(Constants.PACKAGE_LOCATIONS), LCIAMETHODOLOGIES(Constants.PACKAGE_LCIAMETHODOLOGIES),
	LIFECYCLEMODEL(Constants.PACKAGE_LIFECYCLEMODEL);

	ILCDTypes(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	private String value;

	public static ILCDTypes fromValue(String v) {
		for (ILCDTypes c : ILCDTypes.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

	public static ILCDTypes fromReferenceTypeValue(String v) {
		if (Constants.REFERENCE_TYPE_PROCESS.equals(v)) {
			return PROCESS;
		} else if (Constants.REFERENCE_TYPE_FLOW.equals(v)) {
			return FLOW;
		} else if (Constants.REFERENCE_TYPE_FLOW_PROPERTY.equals(v)) {
			return FLOWPROPERTY;
		} else if (Constants.REFERENCE_TYPE_UNIT_GROUP.equals(v)) {
			return UNITGROUP;
		} else if (Constants.REFERENCE_TYPE_LCIA_METHOD.equals(v)) {
			return LCIAMETHOD;
		} else if (Constants.REFERENCE_TYPE_SOURCE.equals(v)) {
			return SOURCE;
		} else if (Constants.REFERENCE_TYPE_CONTACT.equals(v)) {
			return CONTACT;
		} else if (Constants.REFERENCE_TYPE_LIFECYCLEMODEL.equals(v)) {
			return LIFECYCLEMODEL;
		}
		throw new IllegalArgumentException(v);
	}

	/**
	 * 
	 * Determine the type of a given dataset
	 * 
	 * @param obj the dataset
	 * 
	 * @return the type
	 */
	@Deprecated
	public static ILCDTypes determineType(Object obj) {
		if (obj instanceof ProcessDataSet) {
			return ILCDTypes.PROCESS;
		} else if (obj instanceof FlowDataSet) {
			return ILCDTypes.FLOW;
		} else if (obj instanceof FlowPropertyDataSet) {
			return ILCDTypes.FLOWPROPERTY;
		} else if (obj instanceof UnitGroupDataSet) {
			return ILCDTypes.UNITGROUP;
		} else if (obj instanceof SourceDataSet) {
			return ILCDTypes.SOURCE;
		} else if (obj instanceof ContactDataSet) {
			return ILCDTypes.CONTACT;
		} else if (obj instanceof LCIAMethodDataSet) {
			return ILCDTypes.LCIAMETHOD;
		} else if (obj instanceof LifeCycleModelDataSet) {
			return ILCDTypes.LIFECYCLEMODEL;
		} else
			return null;
	}

	/**
	 * 
	 * Determine the type of a given dataset from its root element name
	 * 
	 * @param rootElementName the name of the root element
	 * @return the type
	 * 
	 */
	public static ILCDTypes determineType(String rootElementName) {
		if (rootElementName.equals(Constants.PROCESS_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.PROCESS;
		} else if (rootElementName.equals(Constants.FLOW_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.FLOW;
		} else if (rootElementName.equals(Constants.FLOW_PROPERTY_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.FLOWPROPERTY;
		} else if (rootElementName.equals(Constants.UNIT_GROUP_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.UNITGROUP;
		} else if (rootElementName.equals(Constants.SOURCE_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.SOURCE;
		} else if (rootElementName.equals(Constants.CONTACT_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.CONTACT;
		} else if (rootElementName.equals(Constants.LCIAMETHOD_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.LCIAMETHOD;
		} else if (rootElementName.equals(Constants.CATEGORIES_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.CATEGORIES;
		} else if (rootElementName.equals(Constants.LOCATIONS_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.LOCATIONS;
		} else if (rootElementName.equals(Constants.LCIAMETHODOLOGIES_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.LCIAMETHODOLOGIES;
		} else if (rootElementName.equals(Constants.LIFECYCLEMODEL_ROOT_ELEMENT_NAME)) {
			return ILCDTypes.LIFECYCLEMODEL;
		} else {
			return null;
		}
	}

	/**
	 * Returns the namespace URI for this type.
	 * 
	 * @return the namespace URI
	 */
	public String getNameSpaceURI() {
		switch (this) {
		case PROCESS:
			return Constants.NS_PROCESS;
		case FLOW:
			return Constants.NS_FLOW;
		case FLOWPROPERTY:
			return Constants.NS_FLOWPROPERTY;
		case UNITGROUP:
			return Constants.NS_UNITGROUP;
		case SOURCE:
			return Constants.NS_SOURCE;
		case CONTACT:
			return Constants.NS_CONTACT;
		case LCIAMETHOD:
			return Constants.NS_LCIAMETHOD;
		case LIFECYCLEMODEL:
			return Constants.NS_LIFECYCLEMODEL;
		default:
			return null;
		}
	}

	/**
	 * Returns the name of the root element for this type.
	 * 
	 * @return the name of the root element
	 */
	public String getRootElementName() {
		switch (this) {
		case PROCESS:
			return Constants.PROCESS_ROOT_ELEMENT_NAME;
		case FLOW:
			return Constants.FLOW_ROOT_ELEMENT_NAME;
		case FLOWPROPERTY:
			return Constants.FLOW_PROPERTY_ROOT_ELEMENT_NAME;
		case UNITGROUP:
			return Constants.UNIT_GROUP_ROOT_ELEMENT_NAME;
		case SOURCE:
			return Constants.SOURCE_ROOT_ELEMENT_NAME;
		case CONTACT:
			return Constants.CONTACT_ROOT_ELEMENT_NAME;
		case LCIAMETHOD:
			return Constants.LCIAMETHOD_ROOT_ELEMENT_NAME;
		case LIFECYCLEMODEL:
			return Constants.LIFECYCLEMODEL_ROOT_ELEMENT_NAME;
		default:
			return null;
		}
	}

	/**
	 * Returns the name of the schema for this dataset type (using the values
	 * defined in {@link de.fzk.iai.ilcd.api.Constants}).
	 * 
	 * @return the schema name
	 */
	public String getSchemaName() {
		switch (this) {
		case PROCESS:
			return Constants.PROCESS_SCHEMA_NAME;
		case FLOW:
			return Constants.FLOW_SCHEMA_NAME;
		case FLOWPROPERTY:
			return Constants.FLOW_PROPERTY_SCHEMA_NAME;
		case UNITGROUP:
			return Constants.UNIT_GROUP_SCHEMA_NAME;
		case SOURCE:
			return Constants.SOURCE_SCHEMA_NAME;
		case CONTACT:
			return Constants.CONTACT_SCHEMA_NAME;
		case LCIAMETHOD:
			return Constants.LCIAMETHOD_SCHEMA_NAME;
		case LIFECYCLEMODEL:
			return Constants.LIFECYCLEMODEL_SCHEMA_NAME;
		default:
			return null;
		}
	}

	/**
	 * Returns the name of the XSLT stylesheet for this dataset type (using the
	 * values defined in {@link de.fzk.iai.ilcd.api.Constants}).
	 * 
	 * @return the stylesheet name
	 */
	public String getStylesheetName() {
		switch (this) {
		case PROCESS:
			return Constants.PROCESS_STYLESHEET_NAME;
		case FLOW:
			return Constants.FLOW_STYLESHEET_NAME;
		case FLOWPROPERTY:
			return Constants.FLOWPROPERTY_STYLESHEET_NAME;
		case UNITGROUP:
			return Constants.UNITGROUP_STYLESHEET_NAME;
		case SOURCE:
			return Constants.SOURCE_STYLESHEET_NAME;
		case CONTACT:
			return Constants.CONTACT_STYLESHEET_NAME;
		case LCIAMETHOD:
			return Constants.LCIAMETHOD_STYLESHEET_NAME;
		case LIFECYCLEMODEL:
			return Constants.LIFECYCLEMODEL_SCHEMA_NAME;
		default:
			return null;
		}
	}
}
