/**
 * 
 */
package de.fzk.iai.ilcd.api.binding.helper;

import com.sun.org.apache.xerces.internal.dom.DOMInputImpl;
import de.fzk.iai.ilcd.api.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.ls.LSResourceResolver;

import java.io.InputStream;

class Resolver implements LSResourceResolver {

	private Logger logger = LogManager.getLogger(this.getClass());

	/**
	 * 
	 * @param str
	 * @param str1
	 * @param str2
	 * @param str3
	 * @param str4
	 * @return
	 */
	public org.w3c.dom.ls.LSInput resolveResource(String str, String str1, String str2, String str3, String str4) {
		logger.debug("resolving " + str3 + " to " + Constants.SCHEMAS_PATH_PREFIX + str3);
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(Constants.SCHEMAS_PATH_PREFIX + str3);
		return new DOMInputImpl(str2, str3, str4, is, Constants.DEFAULT_ENCODING);
	}
}