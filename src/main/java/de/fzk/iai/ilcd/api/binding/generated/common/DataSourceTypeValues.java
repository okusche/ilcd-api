//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-34 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.03.01 at 01:55:00 PM MEZ 
//

package de.fzk.iai.ilcd.api.binding.generated.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for DataSourceTypeValues.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="DataSourceTypeValues"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Primary"/&gt;
 *     &lt;enumeration value="&gt; 90% primary"/&gt;
 *     &lt;enumeration value="Mixed primary / secondary"/&gt;
 *     &lt;enumeration value="Secondary"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DataSourceTypeValues")
@XmlEnum
public enum DataSourceTypeValues {

	/**
	 * Data stems exclusively from primary data source(s), i.e. was collected
	 * from owner or operator of the process (independently how the data was
	 * derived).
	 * 
	 */
	@XmlEnumValue("Primary")
	VALUE_1("Primary"),

	/**
	 * Data was averaged from mixed primary and secondary sources, either before
	 * entering the data into a unit process or when calculating the LCI
	 * results. Over 90% of the averaged amount stems from primary data sources.
	 * 
	 */
	@XmlEnumValue("> 90% primary")
	VALUE_2("> 90% primary"),

	/**
	 * Data was averaged from mixed primary and secondary sources with above 10%
	 * secondary data, either before entering the data into a unit process or
	 * when calculating the LCI results.
	 * 
	 */
	@XmlEnumValue("Mixed primary / secondary")
	VALUE_3("Mixed primary / secondary"),

	/**
	 * Data stems exclusively from secondary data source(s), i.e. from any type
	 * of publication, that is not co-authored by the owner or operator of the
	 * process (independently how the data was derived).
	 * 
	 */
	@XmlEnumValue("Secondary")
	VALUE_4("Secondary");
	private final String value;

	DataSourceTypeValues(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static DataSourceTypeValues fromValue(String v) {
		for (DataSourceTypeValues c : DataSourceTypeValues.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

	public String getValue() {
		return value;
	}

}
