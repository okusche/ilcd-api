package de.fzk.iai.ilcd.api.app.common;

import javax.xml.namespace.QName;

public class DataQualityIndicator extends de.fzk.iai.ilcd.api.binding.generated.common.DataQualityIndicatorType {

	// support for numericValue attribute from NS http://eplca.jrc.ec.europa.eu/ILCD/Extensions/2017
	
	public static final QName NUMVAL_ATTRIBUTE = new QName("numericValue");
	
	public Double getNumericValue() {
		Double result = null;
		try {
			result = Double.parseDouble(this.getOtherAttributes().get(NUMVAL_ATTRIBUTE));
		} catch (NumberFormatException e) {
		}
		return result;
	}

	public void setNumericValue(Double numericValue) {
		this.getOtherAttributes().put(NUMVAL_ATTRIBUTE, numericValue.toString());
	}
	
}
