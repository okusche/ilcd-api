package de.fzk.iai.ilcd.api.binding.helper;

import de.fzk.iai.ilcd.api.Constants;
import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.api.app.flow.FlowDataSet;
import de.fzk.iai.ilcd.api.app.flowproperty.FlowPropertyDataSet;
import de.fzk.iai.ilcd.api.app.lciamethod.LCIAMethodDataSet;
import de.fzk.iai.ilcd.api.app.process.ProcessDataSet;
import de.fzk.iai.ilcd.api.app.source.SourceDataSet;
import de.fzk.iai.ilcd.api.app.unitgroup.UnitGroupDataSet;
import de.fzk.iai.ilcd.api.dataset.DataSet;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.fzk.iai.ilcd.api.validation.MyValidationEventCollector;
import de.fzk.iai.ilcd.api.validation.ValidatorListener;
import org.apache.logging.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.output.DOMOutputter;
import org.jdom2.transform.JDOMSource;

import javax.xml.bind.*;
import javax.xml.bind.util.ValidationEventCollector;
import javax.xml.transform.Result;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * Provides methods for marshalling/unmarshalling, validation and XSLT transformation of ILCD datasets.
 * 
 */
public class DatasetHelper extends AbstractHelper {

	protected final Logger log = org.apache.logging.log4j.LogManager.getLogger(DatasetHelper.class);

	private URIResolver uriResolver = null;

	/**
	 * Sole constructor.
	 */
	public DatasetHelper() {
		this.uriResolver = new CURIResolver(null);
	}

	/**
	 * Constructor specifiying a URIResolver to be used this class.
	 * 
	 * @param resolver
	 *            the resolver
	 */
	public DatasetHelper(URIResolver resolver) {
		this.uriResolver = resolver;
	}

	/**
	 * Unmarshal an XML input stream to an object graph representation.
	 * 
	 * @param inputStream
	 *            the input stream
	 * @param type
	 *            the type of dataset to be unmarshalled
	 * @return the dataset as an object graph
	 * @throws JAXBException
	 * @throws UnsupportedEncodingException
	 */
	@SuppressWarnings("unchecked")
	public DataSet unmarshal(InputStream inputStream, ILCDTypes type) throws JAXBException,
			UnsupportedEncodingException {

		JAXBContext jc = ContextFactory.getInstance().getContext(type);

		Unmarshaller unmarshaller = jc.createUnmarshaller();

		unmarshaller.setProperty("com.sun.xml.bind.ObjectFactory",
				MetaObjectFactory.getInstance().getObjectFactory(type));

		JAXBElement<Object> poe = (JAXBElement<Object>) unmarshaller.unmarshal(wrapInputStream(inputStream));

		log.debug(poe.getValue().getClass().getName());

		return (DataSet) poe.getValue();

	}

	/**
	 * Unmarshal a document to an object graph representation.
	 * 
	 * @param doc
	 *            the document
	 * @param type
	 *            the type of dataset to be unmarshalled
	 * @return the dataset as an object graph
	 * @throws JAXBException
	 */
	@SuppressWarnings("unchecked")
	public DataSet unmarshal(Document doc, ILCDTypes type) throws JAXBException {

		DataSet result = null;

		JAXBContext jc = ContextFactory.getInstance().getContext(type);

		Unmarshaller unmarshaller = jc.createUnmarshaller();

		unmarshaller.setProperty("com.sun.xml.bind.ObjectFactory",
				MetaObjectFactory.getInstance().getObjectFactory(type));

		JAXBElement<Object> poe;

		try {
			poe = (JAXBElement<Object>) unmarshaller.unmarshal((new DOMOutputter()).output(doc));
			log.debug(poe.getValue().getClass().getName());
			result = (DataSet) poe.getValue();
		} catch (JDOMException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Marshal an object graph representation of a dataset to an XML outputstream
	 * 
	 * @param dataset
	 *            the dataset
	 * @param outputStream
	 *            the outputstream to write to
	 * @throws JAXBException
	 */
	public void marshal(DataSet dataset, OutputStream outputStream) throws JAXBException {

		ILCDTypes type = dataset.getDatasetType();

		Marshaller marshaller = createMarshaller(type);

		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		marshaller.setProperty("com.sun.xml.bind.xmlHeaders",
				"<?xml-stylesheet type='text/xsl' href='../../stylesheets/" + type.getStylesheetName() + "'?>");

		marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
				type.getNameSpaceURI() + " ../../schemas/" + type.getSchemaName());

		try {
			marshal(dataset, marshaller, outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

	/**
	 * Validate the object graph representation of a dataset against its XML Schema
	 * 
	 * @param dataset
	 *            the dataset
	 * @return a ValidationEventCollector containing all validation events
	 */
	public ValidationEventCollector validate(DataSet dataset) {

		ValidationEventCollector collector = new MyValidationEventCollector();

		ILCDTypes type = dataset.getDatasetType();

		log.debug(type.getValue());

		try {
			Marshaller marshaller = createMarshaller(type);

			SchemaFactory schemaFactory = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);

			log.debug("loading schema document " + Constants.SCHEMAS_PATH_PREFIX + type.getSchemaName());

			Schema schema = schemaFactory.newSchema(Thread.currentThread().getContextClassLoader()
					.getResource(Constants.SCHEMAS_PATH_PREFIX + type.getSchemaName()));

			marshaller.setSchema(schema);
			marshaller.setEventHandler(collector);

			marshal(dataset, marshaller, new NullOutputStream());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		}
		return collector;
	}

	protected void marshal(DataSet dataset, Marshaller marshaller, OutputStream outputStream) throws JAXBException {

		JAXBElement<? extends DataSet> poElement = createJAXBElement(dataset);
		marshaller.marshal(poElement, outputStream);
	}

	/**
	 * Create new dataset of the given type.
	 * 
	 * @param type
	 *            the type
	 * @return the dataset
	 */
	public DataSet createDataset(ILCDTypes type) {

		Object result = null;

		switch (type) {
		case PROCESS:
			result = ((de.fzk.iai.ilcd.api.binding.generated.process.ObjectFactory) MetaObjectFactory.getInstance()
					.getObjectFactory(type)).createProcessDataSetType();
			break;
		case FLOW:
			result = ((de.fzk.iai.ilcd.api.binding.generated.flow.ObjectFactory) MetaObjectFactory.getInstance()
					.getObjectFactory(type)).createFlowDataSetType();
			break;
		case FLOWPROPERTY:
			result = ((de.fzk.iai.ilcd.api.binding.generated.flowproperty.ObjectFactory) MetaObjectFactory
					.getInstance().getObjectFactory(type)).createFlowPropertyDataSetType();
			break;
		case UNITGROUP:
			result = ((de.fzk.iai.ilcd.api.binding.generated.unitgroup.ObjectFactory) MetaObjectFactory.getInstance()
					.getObjectFactory(type)).createUnitGroupDataSetType();
			break;
		case SOURCE:
			result = ((de.fzk.iai.ilcd.api.binding.generated.source.ObjectFactory) MetaObjectFactory.getInstance()
					.getObjectFactory(type)).createSourceDataSetType();
			break;
		case CONTACT:
			result = ((de.fzk.iai.ilcd.api.binding.generated.contact.ObjectFactory) MetaObjectFactory.getInstance()
					.getObjectFactory(type)).createContactDataSetType();
			break;
		case LCIAMETHOD:
			result = ((de.fzk.iai.ilcd.api.binding.generated.lciamethod.ObjectFactory) MetaObjectFactory.getInstance()
					.getObjectFactory(type)).createLCIAMethodDataSetType();
			break;
		default:
			result = null;
			break;
		}

		try {
			Method setVersion = result.getClass().getMethod("setVersion", String.class);
			setVersion.invoke(result, Constants.FORMAT_VERSION);
		} catch (Exception e) {
		}

		try {
			Method setLocations = result.getClass().getMethod("setLocations", String.class);
			setLocations.invoke(result, "../" + Constants.DEFAULT_ILCD_LOCATIONS_NAME);
		} catch (Exception e) {
		}

		try {
			Method setCategories = result.getClass().getMethod("setCategories", String.class);
			setCategories.invoke(result, "../" + Constants.DEFAULT_ILCD_CATEGORIES_NAME);
		} catch (Exception e) {
		}

		return (DataSet) result;
	}

	/**
	 * Wraps a dataset into a JAXBElement.
	 * 
	 * @param dataset
	 *            the dataset
	 * @return the JAXBElement
	 */
	@SuppressWarnings("unchecked")
	public <T extends DataSet> JAXBElement<T> createJAXBElement(DataSet dataset) {

		JAXBElement<T> result = null;

		ILCDTypes type = dataset.getDatasetType();

		Object objectFactory = MetaObjectFactory.getInstance().getObjectFactory(type);

		switch (type) {
		case PROCESS:
			result = (JAXBElement<T>) ((de.fzk.iai.ilcd.api.binding.generated.process.ObjectFactory) objectFactory)
					.createProcessDataSet((ProcessDataSet) dataset);
			break;
		case FLOW:
			result = (JAXBElement<T>) ((de.fzk.iai.ilcd.api.binding.generated.flow.ObjectFactory) MetaObjectFactory
					.getInstance().getObjectFactory(type)).createFlowDataSet((FlowDataSet) dataset);
			break;
		case FLOWPROPERTY:
			result = (JAXBElement<T>) ((de.fzk.iai.ilcd.api.binding.generated.flowproperty.ObjectFactory) MetaObjectFactory
					.getInstance().getObjectFactory(type)).createFlowPropertyDataSet((FlowPropertyDataSet) dataset);
			break;
		case UNITGROUP:
			result = (JAXBElement<T>) ((de.fzk.iai.ilcd.api.binding.generated.unitgroup.ObjectFactory) MetaObjectFactory
					.getInstance().getObjectFactory(type)).createUnitGroupDataSet((UnitGroupDataSet) dataset);
			break;
		case SOURCE:
			result = (JAXBElement<T>) ((de.fzk.iai.ilcd.api.binding.generated.source.ObjectFactory) MetaObjectFactory
					.getInstance().getObjectFactory(type)).createSourceDataSet((SourceDataSet) dataset);
			break;
		case CONTACT:
			result = (JAXBElement<T>) ((de.fzk.iai.ilcd.api.binding.generated.contact.ObjectFactory) MetaObjectFactory
					.getInstance().getObjectFactory(type)).createContactDataSet((ContactDataSet) dataset);
			break;
		case LCIAMETHOD:
			result = (JAXBElement<T>) ((de.fzk.iai.ilcd.api.binding.generated.lciamethod.ObjectFactory) MetaObjectFactory
					.getInstance().getObjectFactory(type)).createLCIAMethodDataSet((LCIAMethodDataSet) dataset);
			break;
		default:
			result = null;
			break;
		}

		return result;
	}

	/**
	 * Validate a dataset against the ILCD Validation XSLT stylesheet
	 * 
	 * @param dataset
	 *            the dataset
	 * 
	 * @return a list of validation events
	 */
	public List<String> ilcdValidate(DataSet dataset, Map<String, String> params) {
		return xsltTransform(dataset, Constants.VALIDATE_STYLESHEET_NAME, params,
				new javax.xml.transform.stream.StreamResult(new NullOutputStream()));
	}

	/**
	 * Validate a dataset against the ILCD Compliance XSLT stylesheet
	 * 
	 * @param dataset
	 *            the dataset
	 * 
	 * @return a list of validation events
	 */
	public List<String> ilcdCompliance(DataSet dataset, Map<String, String> params) {
		return xsltTransform(dataset, Constants.COMPLIANCE_STYLESHEET_NAME, params,
				new javax.xml.transform.stream.StreamResult(new NullOutputStream()));
	}

	/**
	 * Perform an XSLT transformation on the object graph representation of a given dataset
	 * 
	 * @param dataset
	 *            the dataset
	 * @param stylesheet
	 *            the XSLT stylesheet to be used for the transformation
	 * @param XSLResult
	 *            the result
	 * @return a list of any error messages the transformer may have generated
	 */
	public List<String> xsltTransform(DataSet dataset, String stylesheet, Map<String, String> params, Result XSLResult) {

		ValidatorListener h = new ValidatorListener();

		try {

			ILCDTypes type = dataset.getDatasetType();

			log.debug(type.getValue());

			DOMResult result = new DOMResult();

			Marshaller m = createMarshaller(type);

			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

			JAXBElement<? extends DataSet> poElement = createJAXBElement(dataset);
			m.marshal(poElement, result);

			DOMSource source = new DOMSource();

			source.setNode(result.getNode());

			javax.xml.transform.TransformerFactory tFactory = javax.xml.transform.TransformerFactory.newInstance();

			tFactory.setURIResolver(this.uriResolver);

			StreamSource src = new javax.xml.transform.stream.StreamSource(Thread.currentThread()
					.getContextClassLoader().getResourceAsStream(Constants.STYLESHEETS_PATH_PREFIX + stylesheet));

			log.debug("compiling stylesheet " + Constants.STYLESHEETS_PATH_PREFIX + stylesheet);
			log.debug("streamsource is null: " + (src == null));

			javax.xml.transform.Transformer transformer = tFactory.newTransformer(src);

			if (params != null) {
				for (String key : params.keySet()) {
					transformer.setParameter(key, params.get(key));
				}
			}

			transformer.setErrorListener(h);

			log.debug("transformer null? " + transformer == null);
			log.debug("source null? " + source == null);

			transformer.transform(source, XSLResult);

			return h.getResults();

		} catch (Exception e) {
			h.getResults().add("Validation could not be completed due to an error.");
			log.error(e);
		}

		return h.getResults();
	}

	/**
	 * Perform an XSLT transformation on a Document
	 * 
	 * @param doc
	 *            the document
	 * @param stylesheet
	 *            the path of the XSLT stylesheet to be used for the transformation
	 * @param result
	 *            the result
	 * @return a list of any error messages the transformer may have generated
	 */
	public List<String> xsltTransform(Document doc, String stylesheet, Result result) {

		javax.xml.transform.TransformerFactory tFactory = javax.xml.transform.TransformerFactory.newInstance();

		tFactory.setURIResolver(this.uriResolver);

		StreamSource src = new javax.xml.transform.stream.StreamSource(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(Constants.TOOLS_STYLESHEETS_PATH_PREFIX + stylesheet));

		log.debug("compiling stylesheet " + Constants.TOOLS_STYLESHEETS_PATH_PREFIX + stylesheet);
		log.debug("streamsource is null: " + (src == null));

		try {
			javax.xml.transform.Transformer transformer = tFactory.newTransformer(src);

			ValidatorListener h = new ValidatorListener();
			transformer.setErrorListener(h);

			log.debug("transformer null? " + transformer == null);

			transformer.transform(new JDOMSource(doc), result);

			return h.getResults();
		} catch (TransformerConfigurationException e1) {
			log.error(e1);
		} catch (TransformerException e) {
			log.error(e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Returns the URIResolver.
	 * 
	 * @return
	 */
	public URIResolver getUriResolver() {
		return uriResolver;
	}

	/**
	 * Sets the URIResolver.
	 * 
	 * @param uriResolver
	 */
	public void setUriResolver(URIResolver uriResolver) {
		this.uriResolver = uriResolver;
	}

}
