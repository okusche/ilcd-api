package de.fzk.iai.ilcd.api.dataset;

import de.fzk.iai.ilcd.api.Constants;
import de.fzk.iai.ilcd.api.app.common.MultiLangStringList;
import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.api.app.flow.FlowDataSet;
import de.fzk.iai.ilcd.api.app.flowproperty.FlowPropertyDataSet;
import de.fzk.iai.ilcd.api.app.lciamethod.LCIAMethodDataSet;
import de.fzk.iai.ilcd.api.app.lifecyclemodel.LifeCycleModelDataSet;
import de.fzk.iai.ilcd.api.app.process.ProcessDataSet;
import de.fzk.iai.ilcd.api.app.source.SourceDataSet;
import de.fzk.iai.ilcd.api.app.unitgroup.UnitGroupDataSet;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.annotation.XmlTransient;

public class DataSet {
	@XmlTransient
	private final Logger log = org.apache.logging.log4j.LogManager.getLogger(DataSet.class);

	@XmlTransient
	private String migratedFrom = null;

	/**
	 * Determine the type of this dataset
	 * 
	 * @return the type
	 */
	public ILCDTypes getDatasetType() {
		if (this instanceof ProcessDataSet) {
			return ILCDTypes.PROCESS;
		} else if (this instanceof FlowDataSet) {
			return ILCDTypes.FLOW;
		} else if (this instanceof FlowPropertyDataSet) {
			return ILCDTypes.FLOWPROPERTY;
		} else if (this instanceof UnitGroupDataSet) {
			return ILCDTypes.UNITGROUP;
		} else if (this instanceof SourceDataSet) {
			return ILCDTypes.SOURCE;
		} else if (this instanceof ContactDataSet) {
			return ILCDTypes.CONTACT;
		} else if (this instanceof LCIAMethodDataSet) {
			return ILCDTypes.LCIAMETHOD;
		} else if (this instanceof LifeCycleModelDataSet) {
			return ILCDTypes.LIFECYCLEMODEL;
		} else
			return null;
	}

	/**
	 * Determine the type of this dataset
	 * 
	 * @return the type
	 */
	public String getDatasetTypeForReference() {
		if (this instanceof ProcessDataSet) {
			return Constants.REFERENCE_TYPE_PROCESS;
		} else if (this instanceof FlowDataSet) {
			return Constants.REFERENCE_TYPE_FLOW;
		} else if (this instanceof FlowPropertyDataSet) {
			return Constants.REFERENCE_TYPE_FLOW_PROPERTY;
		} else if (this instanceof UnitGroupDataSet) {
			return Constants.REFERENCE_TYPE_UNIT_GROUP;
		} else if (this instanceof SourceDataSet) {
			return Constants.REFERENCE_TYPE_SOURCE;
		} else if (this instanceof ContactDataSet) {
			return Constants.REFERENCE_TYPE_CONTACT;
		} else if (this instanceof LCIAMethodDataSet) {
			return Constants.REFERENCE_TYPE_LCIA_METHOD;
		} else if (this instanceof LifeCycleModelDataSet) {
			return Constants.REFERENCE_TYPE_LIFECYCLEMODEL;
		} else
			return null;
	}

	/**
	 * Return the shortdesc string for this dataset
	 * 
	 * @return the type
	 */
	public String getShortDesc() {

		String result = null;

		try {
			if (this instanceof ProcessDataSet) {
				ProcessDataSet ds = (ProcessDataSet) this;
				result = ((MultiLangStringList<?>) ds.getProcessInformation().getDataSetInformation().getName().getBaseName()).getValue();
			} else if (this instanceof FlowDataSet) {
				FlowDataSet ds = (FlowDataSet) this;
				result = ((MultiLangStringList<?>) ds.getFlowInformation().getDataSetInformation().getName().getBaseName()).getValue();
			} else if (this instanceof FlowPropertyDataSet) {
				FlowPropertyDataSet ds = (FlowPropertyDataSet) this;
				result = ((MultiLangStringList<?>) ds.getFlowPropertiesInformation().getDataSetInformation().getName()).getValue();
			} else if (this instanceof UnitGroupDataSet) {
				UnitGroupDataSet ds = (UnitGroupDataSet) this;
				result = ((MultiLangStringList<?>) ds.getUnitGroupInformation().getDataSetInformation().getName()).getValue();
			} else if (this instanceof SourceDataSet) {
				SourceDataSet ds = (SourceDataSet) this;
				result = ((MultiLangStringList<?>) ds.getSourceInformation().getDataSetInformation().getShortName()).getValue();
			} else if (this instanceof ContactDataSet) {
				ContactDataSet ds = (ContactDataSet) this;
				result = ((MultiLangStringList<?>) ds.getContactInformation().getDataSetInformation().getName()).getValue();
			} else if (this instanceof LCIAMethodDataSet) {
				LCIAMethodDataSet ds = (LCIAMethodDataSet) this;
				result = ((MultiLangStringList<?>) ds.getLCIAMethodInformation().getDataSetInformation().getName()).getValue();
			}
			else if (this instanceof LifeCycleModelDataSet) {
				LifeCycleModelDataSet ds = (LifeCycleModelDataSet) this;
//				result = ((MultiLangStringList<?>) ds.getLifeCycleModelInformation().getDataSetInformation().getName()).getValue();
//				result = ((MultiLangStringList<?>) ds.getLifeCycleModelInformation().getDataSetInformation().getGeneralComment()).getValue();
				result = ds.getLifeCycleModelInformation().getDataSetInformation().getGeneralComment().getValue();
				
//				ds.getLifeCycleModelInformation().getDataSetInformation().getGeneralComment()
			}
		} catch (Exception e) {
			log.error(e);
		}
		return result;
	}

	/**
	 * @return the migratedFrom
	 */
	@XmlTransient
	public String getMigratedFrom() {
		return migratedFrom;
	}

	/**
	 * @param migratedFrom
	 *            the migratedFrom to set
	 */
	public void setMigratedFrom(String migratedFrom) {
		this.migratedFrom = migratedFrom;
	}

}
